package hz.hafidz.footballapps.ui

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.RootMatchers
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.ui.mainactivity.MainActivity
import org.hamcrest.CoreMatchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testAppBehaviour(){
        Espresso.onView(ViewMatchers.withId(R.id.next_match)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Thread.sleep(5000)
        Espresso.onView(ViewMatchers.withId(R.id.coming_matchRecycler)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(R.id.coming_matchRecycler)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                        3, ViewActions.click()))
        Thread.sleep(3000)
        Espresso.onView(ViewMatchers.withId(R.id.menuFavorite)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText(R.string.added_favorite)).inRoot(RootMatchers.withDecorView(CoreMatchers.not(activityRule.activity.window.decorView)))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.pressBack()

//        menunggu selama 5 detik untuk loading
//        mengklik item posisi ke-3
//        menunggu selama 3 detik untuk loading halaman detail
//        mengklik tombol favorite
//        check apakah ada text "Added to Favorite" jika tidak ada, maka berhenti
//        menekan tombol back
    }
}