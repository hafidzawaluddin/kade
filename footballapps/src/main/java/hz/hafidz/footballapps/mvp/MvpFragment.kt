package hz.hafidz.footballapps.mvp

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import hz.hafidz.footballapps.base.BasePresenter

abstract class MvpFragment<P : BasePresenter<*>> : Fragment() {
    protected lateinit var mPresenter: P

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mPresenter = createPresenter()
        return inflater.inflate(getFragmentLayout(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doCreateFragment()
    }

    override fun onStart() {
        super.onStart()
        createPresenter()
    }

    abstract fun doCreateFragment()

    @LayoutRes
    abstract fun getFragmentLayout(): Int

    protected abstract fun createPresenter(): P

    fun showProgress(view: View, progressBar: ProgressBar) {
        view.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgress(view: View, progressBar: ProgressBar){
        view.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }
}