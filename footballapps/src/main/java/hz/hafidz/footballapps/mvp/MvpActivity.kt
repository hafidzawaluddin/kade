package hz.hafidz.footballapps.mvp

import android.os.Bundle
import hz.hafidz.footballapps.base.BaseActivity
import hz.hafidz.footballapps.base.BasePresenter

abstract class MvpActivity<P : BasePresenter<*>> : BaseActivity() {

    protected lateinit var mPresenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        mPresenter = createPresenter()
        super.onCreate(savedInstanceState)
    }

    protected abstract fun createPresenter(): P

    override fun onDestroy() {
        super.onDestroy()
        if (mPresenter != null) {
            mPresenter.detachView()
        }
    }
}