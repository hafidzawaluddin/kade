package hz.hafidz.footballapps.util

object Network {
    const val ID = "id"
    const val T = "t"
    const val E = "e"
    const val NEXT_EVENT = "eventsnextleague.php"
    const val LAST_EVENT = "eventspastleague.php"
    const val DETAIL_EVENT = "lookupevent.php"
    const val TEAM_DETAIL = "lookupteam.php"
    const val LIST_LEAGUE = "all_leagues.php"
    const val LIST_TEAM = "lookup_all_teams.php"
    const val LIST_PLAYER = "lookup_all_players.php"
    const val PLAYERS_DETAIL = "lookupplayer.php"
    const val SEARCH_TEAM = "searchteams.php"
    const val SEARCH_EVENT = "searchevents.php"
}

object IntentParams {
    const val ID_EVENT = "id_event"
    const val ID_LEAGUE = "id_league"
    const val ID_PLAYER = "id_player"
}