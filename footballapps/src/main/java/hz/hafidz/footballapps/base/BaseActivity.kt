package hz.hafidz.footballapps.base

import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar

open class BaseActivity : AppCompatActivity() {

    fun showProgress(view: View, progressBar: ProgressBar) {
        view.visibility = View.INVISIBLE
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgress(view: View, progressBar: ProgressBar){
        view.visibility = View.VISIBLE
        progressBar.visibility = View.INVISIBLE
    }



}