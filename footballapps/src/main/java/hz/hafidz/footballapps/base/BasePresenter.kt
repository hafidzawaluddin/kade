package hz.hafidz.footballapps.base

import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService

open class BasePresenter<V> {
    var mView: V? = null
    protected lateinit var mService: RetrofitService

    fun attachView(mView: V, mService: RetrofitService){
        this.mView = mView
        this.mService = mService
    }

    fun detachView(){
        this.mView = null
    }
}