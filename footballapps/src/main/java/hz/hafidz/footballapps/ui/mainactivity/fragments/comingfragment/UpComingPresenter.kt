package hz.hafidz.footballapps.ui.mainactivity.fragments.comingfragment

import android.annotation.SuppressLint
import hz.hafidz.footballapps.BuildConfig
import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UpComingPresenter(upComingView: UpComingView, retrofitService: RetrofitService) : BasePresenter<UpComingView>() {
    init {
        attachView(upComingView, retrofitService)
    }

    @SuppressLint("CheckResult")
    fun getNextMatch(leagueId:String) {
        mView?.showLoading()
        mService.getNextMatch(leagueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                        mView?.onSuccess(it.events)
                        mView?.hideLoading()
                }
    }

    @SuppressLint("CheckResult")
    fun getSearch(event:String?){
        mService.getSearchEvent(event)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    mView?.onSearchSuccess(it)
                }
    }
}