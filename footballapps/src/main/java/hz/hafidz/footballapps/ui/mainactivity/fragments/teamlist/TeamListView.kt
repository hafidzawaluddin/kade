package hz.hafidz.footballapps.ui.mainactivity.fragments.teamlist

import hz.hafidz.footballapps.network.dao.LeagueDao
import hz.hafidz.footballapps.network.dao.ListTeam
import hz.hafidz.footballapps.network.dao.TeamsItem

interface TeamListView {
    fun onSuccess(mModel: List<TeamsItem?>?)
    fun onSuccessTeam(mModel: LeagueDao)
     fun onSearchSuccess(it: ListTeam?)
}