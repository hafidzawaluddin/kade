package hz.hafidz.footballapps.ui.detailteam.fragment.playerlist

import android.annotation.SuppressLint
import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PlayerListPresenter(playerListView: PlayerListView, retrofitService: RetrofitService): BasePresenter<PlayerListView>() {
    init {
        attachView(playerListView, retrofitService)
    }

    @SuppressLint("CheckResult")
    fun getListPlayer(id: String?){
        mService.getListPlayer(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    mView?.onSuccess(it)
                }
    }
}