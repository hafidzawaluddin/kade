package hz.hafidz.footballapps.ui.mainactivity.fragments.favoriteetamfragment

import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService

class FavoriteTeamPresenter(favoriteTeamView: FavoriteTeamView, retrofitService: RetrofitService): BasePresenter<FavoriteTeamView>() {
    init {
        attachView(favoriteTeamView, retrofitService)
    }

}