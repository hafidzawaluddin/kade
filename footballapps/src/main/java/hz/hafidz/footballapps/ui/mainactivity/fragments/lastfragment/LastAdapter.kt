package hz.hafidz.footballapps.ui.mainactivity.fragments.lastfragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.network.dao.MatchDaoList
import kotlinx.android.synthetic.main.item_match.view.*

class LastAdapter(private val models: MutableList<MatchDaoList?>, private val listener: (MatchDaoList) -> Unit) : RecyclerView.Adapter<LastAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.item_match, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        models[p1]?.let { p0.bindItem(it, listener) }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(model: MatchDaoList, listener: (MatchDaoList) -> Unit) {
            itemView.txtDate.text = model.strDate
            itemView.txtTeamHome.text = model.strHomeTeam
            itemView.txtHomeScore.text = model.intHomeScore.toString()
            itemView.txtTeamAway.text = model.strAwayTeam
            itemView.txtAwayScore.text = model.intAwayScore.toString()
            itemView.setOnClickListener {
                listener(model)
            }
        }
    }
}