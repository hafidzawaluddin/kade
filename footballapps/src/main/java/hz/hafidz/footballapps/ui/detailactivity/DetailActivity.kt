package hz.hafidz.footballapps.ui.detailactivity

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.db.database
import hz.hafidz.footballapps.mvp.MvpActivity
import hz.hafidz.footballapps.network.dao.DetailEvent
import hz.hafidz.footballapps.network.dao.EventsItem
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.mainactivity.fragments.favoritefragment.FavoriteFragment
import hz.hafidz.footballapps.ui.mainactivity.fragments.favoritefragment.FavoriteView
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.toast

class DetailActivity : MvpActivity<DetailPresenter>(), DetailView {

    private lateinit var idEvent: String
    private var menus: Menu? = null
    private var isFavorite: Boolean = false
    private var mDao: EventsItem? = null


    override fun onDetailMatchSuccess(detailEvent: DetailEvent) {
        mDao = detailEvent.events?.get(0)
        txtTeamHome.text = mDao?.strHomeTeam
        txtTeamAway.text = mDao?.strAwayTeam

        txtHomeScore.text = mDao?.intHomeScore
        txtAwayScore.text = mDao?.intAwayScore

        txtHomeGoal.text = mDao?.strHomeGoalDetails
        txtAwayGoal.text = mDao?.strAwayGoalDetails

        txtHomeGoalKeeper.text = mDao?.strHomeLineupGoalkeeper
        txtAwayGoalKeeper.text = mDao?.strAwayLineupGoalkeeper

        txtHomeDefense.text = mDao?.strHomeLineupDefense
        txtAwayDefense.text = mDao?.strAwayLineupDefense

        txtHomeMidfield.text = mDao?.strHomeLineupMidfield
        txtAwayMidfield.text = mDao?.strAwayLineupMidfield

        txtHomeForward.text = mDao?.strHomeLineupForward
        txtAwayForward.text = mDao?.strAwayLineupForward

        txtHomeSubs.text = mDao?.strHomeLineupSubstitutes
        txtAwaySubs.text = mDao?.strAwayLineupSubstitutes

        mPresenter.getImageTeamHome(mDao?.idHomeTeam)
        mPresenter.getImageTeamAway(mDao?.idAwayTeam)
        hideProgress(main, loading)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favorite_menu, menu)
        menus = menu
        isFavorite = database.isFavorite(idEvent)
        isFavorite()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onGetTeamImageAway(badge: String?) {
        Glide.with(this).load(badge).into(imgTeamAway)
    }

    override fun onGetTeamImageHome(strTeamBadge: String?) {
        Glide.with(this).load(strTeamBadge).into(imgTeamHome)
    }

    override fun createPresenter(): DetailPresenter {
        val retrofitService = RetrofitClient.request().create(RetrofitService::class.java)
        return DetailPresenter(this, retrofitService)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        idEvent = intent.getStringExtra(IntentParams.ID_EVENT)
        mPresenter.getDetailMatch(idEvent)
        showProgress(main, loading)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menuFavorite -> {
                if (isFavorite) {
                    database.deleteFavorite(idEvent)
                    isFavorite = !isFavorite
                    isFavorite()
                    toast(getString(R.string.removed_favorite))
                } else {
                    database.insertFavorite(mDao?.idEvent, mDao?.strHomeTeam, mDao?.strAwayTeam,
                            mDao?.intHomeScore, mDao?.intAwayScore, mDao?.dateEvent)
                    isFavorite = database.isFavorite(mDao?.idEvent)
                    isFavorite()
                    toast(getString(R.string.added_favorite))
                }
                true
            }
            R.id.home -> {
                onBackPressed()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun isFavorite() {
        if (isFavorite)
            menus?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp)
        else
            menus?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp)
    }

}
