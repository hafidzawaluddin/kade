package hz.hafidz.footballapps.ui.detailactivity

import android.annotation.SuppressLint
import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailplayer.DetailPlayerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailPresenter(detailView: DetailView, retrofitService: RetrofitService) : BasePresenter<DetailView>() {
    init {
        attachView(detailView, retrofitService)
    }

    @SuppressLint("CheckResult")
    fun getDetailMatch(idEvent: String) {
        mService.getDetailEvent(idEvent)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    run { mView?.onDetailMatchSuccess(it) }
                }
    }

    @SuppressLint("CheckResult")
    fun getImageTeamHome(idTeam: String?) {
        mService.getDetailTeam(idTeam)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    run { mView?.onGetTeamImageHome(it.teams?.get(0)?.strTeamBadge) }
                }
    }
    @SuppressLint("CheckResult")
    fun getImageTeamAway(idTeam: String?) {
        mService.getDetailTeam(idTeam)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    run { mView?.onGetTeamImageAway(it.teams?.get(0)?.strTeamBadge) }
                }
    }
}