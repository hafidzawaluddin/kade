package hz.hafidz.footballapps.ui.mainactivity.fragments.comingfragment

import hz.hafidz.footballapps.network.dao.MatchDaoList
import hz.hafidz.footballapps.network.dao.MatchSearch

interface UpComingView {
    fun onSuccess(events: List<MatchDaoList?>?)
    fun onFailure()
    fun showLoading()
    fun hideLoading()
    fun onSearchSuccess(events: MatchSearch)
}