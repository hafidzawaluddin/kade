package hz.hafidz.footballapps.ui.mainactivity.fragments.comingfragment

import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import hz.hafidz.footballapps.BuildConfig
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.mvp.MvpFragment
import hz.hafidz.footballapps.network.dao.MatchDaoList
import hz.hafidz.footballapps.network.dao.MatchSearch
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailactivity.DetailActivity
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.fragment_coming.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.startActivity

class UpComingFragment : MvpFragment<UpComingPresenter>(), UpComingView, TextWatcher {


    private val models: MutableList<MatchDaoList?> = mutableListOf()
    private lateinit var mAdapter: UpComingRecyclerAdapter

    override fun onSearchSuccess(events: MatchSearch) {
        models.clear()
        events.event?.let { models.addAll(it) }
        mAdapter.notifyDataSetChanged()
    }

    override fun afterTextChanged(p0: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        mPresenter.getSearch(p0.toString())
    }

    override fun showLoading() {
        showProgress(coming_matchRecycler, coming_loading)
    }

    override fun hideLoading() {
        hideProgress(coming_matchRecycler, coming_loading)
    }

    override fun onSuccess(events: List<MatchDaoList?>?) {
        if (events != null) {
            models.clear()
            models.addAll(events)
            mAdapter.notifyDataSetChanged()
        }
    }

    override fun onFailure() {
        context?.getString(R.string.error)?.let { coming_parent.snackbar(it) }
    }


    override fun doCreateFragment() {
        setRecycler()
        mPresenter.getNextMatch(BuildConfig.LEAGUE)
        txtSearch.addTextChangedListener(this)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_coming
    }

    override fun createPresenter(): UpComingPresenter {
        val retrofitService = RetrofitClient.request().create(RetrofitService::class.java)
        return UpComingPresenter(this, retrofitService)
    }

    private fun setRecycler() {
        mAdapter = UpComingRecyclerAdapter(models) {
            startActivity<DetailActivity>(IntentParams.ID_EVENT to it.idEvent)
        }
        coming_matchRecycler.adapter = mAdapter
        coming_matchRecycler.layoutManager = LinearLayoutManager(context)
    }


}