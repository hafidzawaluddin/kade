package hz.hafidz.footballapps.ui.detailplayer

import hz.hafidz.footballapps.network.dao.DetailPlayer

interface DetailPlayerView {
    fun onSuccess(it: DetailPlayer)
}