package hz.hafidz.footballapps.ui.mainactivity.fragments.favoritefragment

import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService

class FavoritePresenter(favoriteView: FavoriteView, retrofitService: RetrofitService): BasePresenter<FavoriteView>() {
    init {
        attachView(favoriteView, retrofitService)
    }
}