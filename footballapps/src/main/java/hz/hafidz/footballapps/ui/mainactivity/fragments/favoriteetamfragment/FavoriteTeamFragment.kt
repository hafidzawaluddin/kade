package hz.hafidz.footballapps.ui.mainactivity.fragments.favoriteetamfragment

import android.support.v7.widget.LinearLayoutManager
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.db.TeamFavorite
import hz.hafidz.footballapps.db.database
import hz.hafidz.footballapps.mvp.MvpFragment
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailteam.DetailTeamActivity
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.fragment_favorite_team.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

class FavoriteTeamFragment: MvpFragment<FavoriteTeamPresenter>(), FavoriteTeamView {

    private  var  models: List<TeamFavorite>? = null

    override fun doCreateFragment() {

        models = activity?.database?.getFavoriteTeam()

        val mAdapter = FavoriteTeamRecyclerAdapter(models){
            startActivity<DetailTeamActivity>(IntentParams.ID_LEAGUE to it?.idTeam)
        }

        favTeamRecycler.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(context)
        }

        mAdapter.notifyDataSetChanged()
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_favorite_team
    }

    override fun createPresenter(): FavoriteTeamPresenter {
        val service = RetrofitClient.request().create(RetrofitService::class.java)
        return FavoriteTeamPresenter(this, service)
    }

}