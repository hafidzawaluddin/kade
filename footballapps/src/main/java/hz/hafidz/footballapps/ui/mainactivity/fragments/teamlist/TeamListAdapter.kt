package hz.hafidz.footballapps.ui.mainactivity.fragments.teamlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.network.dao.TeamsItem
import kotlinx.android.synthetic.main.item_team.view.*

class TeamListAdapter(private val models: List<TeamsItem?>?,
                      private val listener: (TeamsItem?) -> Unit) : RecyclerView.Adapter<TeamListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.item_team, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return models?.size ?: 0
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(models?.get(p1), listener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(model: TeamsItem?, listener: (TeamsItem?) -> Unit){
            Glide.with(itemView).load(model?.strTeamBadge).into(itemView.imgTeam)
            itemView.txtTeam.text = model?.strTeam
            itemView.setOnClickListener { listener(model)  }
        }
    }

}