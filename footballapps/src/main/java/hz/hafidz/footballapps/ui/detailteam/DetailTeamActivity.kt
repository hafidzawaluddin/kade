package hz.hafidz.footballapps.ui.detailteam

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.db.TeamFavorite
import hz.hafidz.footballapps.db.database
import hz.hafidz.footballapps.mvp.MvpActivity
import hz.hafidz.footballapps.network.dao.DetailTeam
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailteam.fragment.detailteam.TeamsDetailFragment
import hz.hafidz.footballapps.ui.detailteam.fragment.playerlist.PlayerListFragment
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.activity_detail_team.*
import org.jetbrains.anko.toast

class DetailTeamActivity : MvpActivity<DetailTeamPresenter>(), DetailTeamView {

    private var menus: Menu? = null
    private var isFavorite: Boolean = false
    private lateinit var model: TeamFavorite

    override fun createPresenter(): DetailTeamPresenter {
        val service = RetrofitClient.request().create(RetrofitService::class.java)
        return DetailTeamPresenter(this, service)
    }

    override fun onSuccess(it: DetailTeam?) {
        val models = it?.teams?.get(0)
        model = TeamFavorite(null, models?.idTeam?.toIntOrNull(), models?.strTeamBadge, models?.strTeam)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_team)

        mPresenter.getDetailTeam(intent.getStringExtra(IntentParams.ID_LEAGUE))
        val vpAdapter = PagerAdapter()
        vpAdapter.addFragment(TeamsDetailFragment())
        vpAdapter.addFragment(PlayerListFragment())
        viewPager.adapter = vpAdapter
        println(vpAdapter.count)
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favorite_menu, menu)
        menus = menu
        isFavorite = database.isTeamFavorite(intent.getStringExtra(IntentParams.ID_LEAGUE))
        isFavorite()
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menuFavorite -> {
                if (isFavorite) {
                    database.deleteFavoriteTeam(model.idTeam.toString())
                    isFavorite = !isFavorite
                    isFavorite()
                    toast(getString(R.string.removed_favorite))
                } else {
                    database.insertTeamFavorite(model.idTeam.toString(), model.img, model.teamName)
                    isFavorite()
                    toast(getString(R.string.added_favorite))
                }
                true
            }
            R.id.home -> {
                onBackPressed()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun isFavorite() {
        if (isFavorite)
            menus?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp)
        else
            menus?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp)
    }

    inner class PagerAdapter : FragmentPagerAdapter(supportFragmentManager) {

        private val mFragmentList = ArrayList<Fragment>()

        override fun getItem(p0: Int): Fragment = mFragmentList[p0]

        override fun getCount(): Int = mFragmentList.size

        override fun getPageTitle(position: Int): CharSequence? {
            return when(position){
                0 ->  "Team Detail"
                1 ->  "List Player"
                else -> ""
            }
        }

        fun addFragment(fragment: Fragment){
            mFragmentList.add(fragment)
        }

    }
}
