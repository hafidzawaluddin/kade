package hz.hafidz.footballapps.ui.mainactivity.fragments.favoritefragment

import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.db.FavoriteTable
import hz.hafidz.footballapps.db.database
import hz.hafidz.footballapps.mvp.MvpFragment
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailactivity.DetailActivity
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.match_fragment.*
import org.jetbrains.anko.support.v4.startActivity

class FavoriteFragment : MvpFragment<FavoritePresenter>(), FavoriteView {

    private var models: List<FavoriteTable>? = null

    override fun doCreateFragment() {
        setRecycler()
        mAdapter.notifyDataSetChanged()
    }

    private lateinit var mAdapter: FavoriteRecyclerAdapter

    override fun getFragmentLayout(): Int =
            R.layout.favorite_layout

    override fun createPresenter(): FavoritePresenter {
        val retrofitService = RetrofitClient.request().create(RetrofitService::class.java)
        return FavoritePresenter(this, retrofitService)
    }


    private fun setRecycler() {
         models = activity?.database?.getFavorite()
            models?.forEach {
                Log.wtf("Models", it.idEvent.toString())
            }
        mAdapter = FavoriteRecyclerAdapter(models) {
            println(it?.idEvent)
            startActivity<DetailActivity>(IntentParams.ID_EVENT to it?.idEvent.toString())
        }
        matchRecycler.adapter = mAdapter
        matchRecycler.layoutManager = LinearLayoutManager(activity)
    }

    override fun onResume() {
        models = activity?.database?.getFavorite()
        mAdapter.notifyDataSetChanged()
        super.onResume()
    }
}