package hz.hafidz.footballapps.ui.mainactivity.fragments.teamlist

import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.mvp.MvpFragment
import hz.hafidz.footballapps.network.dao.LeagueDao
import hz.hafidz.footballapps.network.dao.ListTeam
import hz.hafidz.footballapps.network.dao.TeamsItem
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailteam.DetailTeamActivity
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.fragment_team.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast

class TeamListFragment: MvpFragment<TeamListPresenter>(), TeamListView, AdapterView.OnItemSelectedListener, TextWatcher {

    private val listTeam: MutableList<String?> = mutableListOf()
    private val idList: MutableList<String?> = mutableListOf()
    private val models: MutableList<TeamsItem?>? = mutableListOf()
    private lateinit var mAdapter: TeamListAdapter

    override fun onSearchSuccess(it: ListTeam?) {
        models?.clear()
        it?.teams?.let { it1 -> models?.addAll(it1) }
        mAdapter.notifyDataSetChanged()
    }

    override fun afterTextChanged(p0: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        mPresenter.getSearch(p0.toString())
    }


    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        mPresenter.getTeam(idList[p2])
    }

    override fun onSuccessTeam(mModel: LeagueDao) {
        mModel.leagues?.forEach { idList.add(it?.idLeague) }
        mModel.leagues?.forEach { listTeam.add(it?.strLeague) }
        val adapter = ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item)
        adapter.addAll(listTeam)
        spinner.adapter = adapter
        adapter.notifyDataSetChanged()
        spinner.onItemSelectedListener = this
    }

    override fun onSuccess(mModel: List<TeamsItem?>?) {
        if (mModel != null) {
            models?.clear()
            models?.addAll(mModel)
        }
        mAdapter.notifyDataSetChanged()
        println(mAdapter.itemCount)
    }

    override fun doCreateFragment() {

        mPresenter.getLeague()

        mAdapter = TeamListAdapter(models){
            startActivity<DetailTeamActivity>(IntentParams.ID_LEAGUE to it?.idTeam)
        }

        teamRecycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }

        txtSearch.addTextChangedListener(this)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_team
    }

    override fun createPresenter(): TeamListPresenter {
        val service = RetrofitClient.request().create(RetrofitService::class.java)
        return TeamListPresenter(this, service)
    }

}