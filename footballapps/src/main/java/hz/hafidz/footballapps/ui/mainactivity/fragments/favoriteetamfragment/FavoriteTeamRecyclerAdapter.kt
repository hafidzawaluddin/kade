package hz.hafidz.footballapps.ui.mainactivity.fragments.favoriteetamfragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.db.TeamFavorite
import kotlinx.android.synthetic.main.item_team.view.*

class FavoriteTeamRecyclerAdapter(private val models: List<TeamFavorite>?,
                                  private val listener: (TeamFavorite?) -> Unit): RecyclerView.Adapter<FavoriteTeamRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.item_team, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return models?.size ?: 0
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(models?.get(p1), listener)
    }

    class ViewHolder(private val view: View): RecyclerView.ViewHolder(view){

        fun bindItem(model: TeamFavorite?, listener: (TeamFavorite?) -> Unit){
            Glide.with(view).load(model?.img).into(view.imgTeam)
            view.txtTeam.text = model?.teamName
            view.setOnClickListener { listener(model) }
        }

    }
}