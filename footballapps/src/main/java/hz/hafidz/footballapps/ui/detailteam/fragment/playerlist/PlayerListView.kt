package hz.hafidz.footballapps.ui.detailteam.fragment.playerlist

import hz.hafidz.footballapps.network.dao.ListPlayer

interface PlayerListView {
    fun onSuccess(it: ListPlayer?)
}