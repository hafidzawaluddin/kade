package hz.hafidz.footballapps.ui.detailplayer

import android.os.Bundle
import com.bumptech.glide.Glide
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.mvp.MvpActivity
import hz.hafidz.footballapps.network.dao.DetailPlayer
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.activity_detail_player.*
import org.jetbrains.anko.toast

class DetailPlayerActivity : MvpActivity<DetailPlayerPresenter>(), DetailPlayerView {

    override fun onSuccess(it: DetailPlayer) {
        val model = it.players?.get(0)
        Glide.with(this).load(model?.strCutout).into(imgPlayerDetail)
        txtWeight.text = model?.strWeight
        txtHeight.text = model?.strHeight
        txtPosition.text = model?.strPosition
        txtDetail.text = model?.strDescriptionEN
    }

    override fun createPresenter(): DetailPlayerPresenter {
        val service = RetrofitClient.request().create(RetrofitService::class.java)
        return DetailPlayerPresenter(this, service)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_player)

        mPresenter.getPlayerDetail(intent.getStringExtra(IntentParams.ID_PLAYER))
    }
}
