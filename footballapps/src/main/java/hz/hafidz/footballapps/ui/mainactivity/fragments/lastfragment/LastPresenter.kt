package hz.hafidz.footballapps.ui.mainactivity.fragments.lastfragment

import android.annotation.SuppressLint
import hz.hafidz.footballapps.BuildConfig
import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LastPresenter(lastView: LastView, retrofitService: RetrofitService) : BasePresenter<LastView>() {
    init {
        attachView(lastView, retrofitService)
    }

    @SuppressLint("CheckResult")
    fun getLastMatch() {
        mService.getLastMatch(BuildConfig.LEAGUE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                     run { mView?.onSuccess(it.events) }
                }
    }

    @SuppressLint("CheckResult")
    fun getSearch(event:String?){
        mService.getSearchEvent(event)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    mView?.onSearchSuccess(it)
                }
    }
}