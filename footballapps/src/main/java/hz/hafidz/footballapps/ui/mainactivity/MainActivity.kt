package hz.hafidz.footballapps.ui.mainactivity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.mvp.MvpActivity
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.mainactivity.fragments.comingfragment.UpComingFragment
import hz.hafidz.footballapps.ui.mainactivity.fragments.favoriteetamfragment.FavoriteTeamFragment
import hz.hafidz.footballapps.ui.mainactivity.fragments.favoritefragment.FavoriteFragment
import hz.hafidz.footballapps.ui.mainactivity.fragments.lastfragment.LastFragment
import hz.hafidz.footballapps.ui.mainactivity.fragments.teamlist.TeamListFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpActivity<MainPresenter>(), MainView {

    override fun createPresenter(): MainPresenter {
        val retrofitService = RetrofitClient.request().create(RetrofitService::class.java)
        return MainPresenter(this, retrofitService)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val vpAdapter = PagerAdapter()
        vpAdapter.addFragment(UpComingFragment())
        vpAdapter.addFragment(LastFragment())
        vpAdapter.addFragment(FavoriteFragment())
        vpAdapter.addFragment(TeamListFragment())
        vpAdapter.addFragment(FavoriteTeamFragment())
        vp.adapter = vpAdapter
        vp.offscreenPageLimit = 5

        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.next_match -> vp.currentItem = 0
                R.id.last_match -> vp.currentItem = 1
                R.id.favorite_match -> vp.currentItem = 2
                R.id.team_list -> vp.currentItem = 3
                R.id.team_favorite -> vp.currentItem = 4
            }
            true
        }

        vp.currentItem = 0
        bottomNavigation.selectedItemId = R.id.next_match
    }

    inner class PagerAdapter : FragmentPagerAdapter(supportFragmentManager) {

        private val mFragmentList = ArrayList<Fragment>()

        override fun getItem(p0: Int): Fragment = mFragmentList[p0]

        override fun getCount(): Int = mFragmentList.size

        fun addFragment(fragment:Fragment){
            mFragmentList.add(fragment)
        }

    }
}
