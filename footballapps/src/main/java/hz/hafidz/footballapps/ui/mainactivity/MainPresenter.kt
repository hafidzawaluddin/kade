package hz.hafidz.footballapps.ui.mainactivity

import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService

class MainPresenter(mainView: MainView, retrofitService: RetrofitService) : BasePresenter<MainView>() {
    init {
        attachView(mainView, retrofitService)
    }


}