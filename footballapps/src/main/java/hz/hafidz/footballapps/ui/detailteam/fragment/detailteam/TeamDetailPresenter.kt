package hz.hafidz.footballapps.ui.detailteam.fragment.detailteam

import android.annotation.SuppressLint
import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TeamDetailPresenter(teamDetailVIew: TeamDetailVIew, retrofitService: RetrofitService) : BasePresenter<TeamDetailVIew>() {
    init {
        attachView(teamDetailVIew, retrofitService)
    }

    @SuppressLint("CheckResult")
    fun getDetailTeam(id:String?){
        mService.getDetailTeam(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    mView?.onSuccess(it)
                }
    }
}