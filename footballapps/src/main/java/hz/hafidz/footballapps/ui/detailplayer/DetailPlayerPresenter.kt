package hz.hafidz.footballapps.ui.detailplayer

import android.annotation.SuppressLint
import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailPlayerPresenter(detailPlayerView: DetailPlayerView, retrofitService: RetrofitService): BasePresenter<DetailPlayerView>() {
    init {
        attachView(detailPlayerView, retrofitService)
    }

    @SuppressLint("CheckResult")
    fun getPlayerDetail(id:String?){
        mService.getPlayerDetail(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    mView?.onSuccess(it)
                }
    }
}