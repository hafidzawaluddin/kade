package hz.hafidz.footballapps.ui.detailteam.fragment.detailteam

import com.bumptech.glide.Glide
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.mvp.MvpFragment
import hz.hafidz.footballapps.network.dao.DetailTeam
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.fragment_detail_team.*

class TeamsDetailFragment : MvpFragment<TeamDetailPresenter>(), TeamDetailVIew {


    override fun onSuccess(it: DetailTeam?) {
        val detail = it?.teams?.get(0)
        Glide.with(this).load(detail?.strTeamBadge).into(imgDetail)
        txtName.text = detail?.strTeam
        txtFormed.text = detail?.intFormedYear
        txtDesc.text = detail?.strDescriptionEN
    }

    override fun doCreateFragment() {
        mPresenter.getDetailTeam(activity?.intent?.getStringExtra(IntentParams.ID_LEAGUE))
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_detail_team
    }

    override fun createPresenter(): TeamDetailPresenter {
        val service = RetrofitClient.request().create(RetrofitService::class.java)
        return TeamDetailPresenter(this, service)
    }

}