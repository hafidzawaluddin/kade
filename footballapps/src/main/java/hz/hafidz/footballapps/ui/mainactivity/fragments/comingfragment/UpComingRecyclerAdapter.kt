package hz.hafidz.footballapps.ui.mainactivity.fragments.comingfragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.network.dao.MatchDaoList
import kotlinx.android.synthetic.main.item_match.view.*

class UpComingRecyclerAdapter(private val models: List<MatchDaoList?>, private val listener: (MatchDaoList) -> Unit) : RecyclerView.Adapter<UpComingRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.item_match, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        models[p1]?.let { p0.bindItem(it, listener) }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItem(model: MatchDaoList, listener: (MatchDaoList) -> Unit) {
            itemView.txtDate.text = model.dateEvent
            itemView.txtTeamHome.text = model.strHomeTeam
            itemView.txtHomeScore.text = ""
            itemView.txtHomeScore.visibility = View.GONE
            itemView.txtTeamAway.text = model.strAwayTeam
            itemView.txtAwayScore.text = ""
            itemView.txtAwayScore.visibility = View.GONE
            itemView.setOnClickListener {
                listener(model)
            }
        }

    }
}