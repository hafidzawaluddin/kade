package hz.hafidz.footballapps.ui.mainactivity.fragments.favoritefragment

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.db.FavoriteTable
import kotlinx.android.synthetic.main.item_match.view.*

class FavoriteRecyclerAdapter(private val models: List<FavoriteTable>?, private val listener: (FavoriteTable?) -> Unit) : RecyclerView.Adapter<FavoriteRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.item_match, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return models?.size ?: 0
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        models?.get(p1).let { p0.bindItem(it, listener) }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItem(model: FavoriteTable?, listener: (FavoriteTable?) -> Unit) {
            itemView.txtDate.text = model?.eventDate
            itemView.txtTeamHome.text = model?.teamHome
            itemView.txtHomeScore.text = model?.scoreHome.toString()
            itemView.txtTeamAway.text = model?.teamAway
            itemView.txtAwayScore.text = model?.scoreAway.toString()
            itemView.setOnClickListener {
                listener(model)
            }
        }

    }
}