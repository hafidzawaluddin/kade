package hz.hafidz.footballapps.ui.detailteam.fragment.playerlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.network.dao.PlayerItem
import kotlinx.android.synthetic.main.item_player.view.*

class PlayerListRecyclerAdapter(private val models: List<PlayerItem?>?,
                                private val listener: (PlayerItem?) -> Unit ) : RecyclerView.Adapter<PlayerListRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.item_player, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return models?.size ?: 0
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(models?.get(p1), listener)
    }


    class ViewHolder(private val v: View) : RecyclerView.ViewHolder(v){
        fun bindItem(model: PlayerItem?, listener: (PlayerItem?) -> Unit){
            Glide.with(v).load(model?.strCutout).into(v.imgPlayer)
            v.txtPlayerName.text = model?.strPlayer.toString()
            v.setOnClickListener { listener(model) }
        }
    }
}