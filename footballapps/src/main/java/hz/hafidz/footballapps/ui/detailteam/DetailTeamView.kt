package hz.hafidz.footballapps.ui.detailteam

import hz.hafidz.footballapps.network.dao.DetailTeam

interface DetailTeamView {
    fun onSuccess(it: DetailTeam?)
}