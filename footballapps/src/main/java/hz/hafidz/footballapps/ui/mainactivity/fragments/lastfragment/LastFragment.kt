package hz.hafidz.footballapps.ui.mainactivity.fragments.lastfragment

import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.mvp.MvpFragment
import hz.hafidz.footballapps.network.dao.MatchDaoList
import hz.hafidz.footballapps.network.dao.MatchSearch
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailactivity.DetailActivity
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.fragment_last.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.startActivity

class LastFragment : MvpFragment<LastPresenter>(), LastView, TextWatcher {

    private lateinit var mAdapter: LastAdapter
    private val models: MutableList<MatchDaoList?> = mutableListOf()

    override fun onSearchSuccess(it: MatchSearch?) {
        if (it != null) {
            models.clear()
            it.event?.let { it1 -> models.addAll(it1) }
            mAdapter.notifyDataSetChanged()
        }
    }

    override fun afterTextChanged(p0: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        mPresenter.getSearch(p0.toString())
    }

    override fun onSuccess(events: List<MatchDaoList?>?) {
        if (events != null) {
            models.clear()
            models.addAll(events)
            mAdapter.notifyDataSetChanged()
        }
        hideProgress(last_matchRecycler, last_loading)
    }

    override fun onFailure() {
        context?.getString(R.string.error)?.let { last_parent.snackbar(it) }
    }

    override fun doCreateFragment() {
        setRecycler()
        showProgress(last_matchRecycler, last_loading)

        mPresenter.getLastMatch()
        txtSearch.addTextChangedListener(this)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_last
    }

    override fun createPresenter(): LastPresenter {
        val retrofitService = RetrofitClient.request().create(RetrofitService::class.java)
        return LastPresenter(this, retrofitService)
    }

    private fun setRecycler() {
        mAdapter = LastAdapter(models) {
            startActivity<DetailActivity>(IntentParams.ID_EVENT to it.idEvent)
        }
        last_matchRecycler.layoutManager = LinearLayoutManager(context)
        last_matchRecycler.adapter = mAdapter
    }
}