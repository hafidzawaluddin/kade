package hz.hafidz.footballapps.ui.mainactivity.fragments.lastfragment

import hz.hafidz.footballapps.network.dao.MatchDaoList
import hz.hafidz.footballapps.network.dao.MatchSearch

interface LastView {
    fun onSuccess(events: List<MatchDaoList?>?)
    fun onFailure()
    fun onSearchSuccess(it: MatchSearch?)

}