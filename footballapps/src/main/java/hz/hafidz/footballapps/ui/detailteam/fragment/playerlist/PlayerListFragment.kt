package hz.hafidz.footballapps.ui.detailteam.fragment.playerlist

import android.support.v7.widget.LinearLayoutManager
import hz.hafidz.footballapps.R
import hz.hafidz.footballapps.mvp.MvpFragment
import hz.hafidz.footballapps.network.dao.ListPlayer
import hz.hafidz.footballapps.network.retrofit.RetrofitClient
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailplayer.DetailPlayerActivity
import hz.hafidz.footballapps.util.IntentParams
import kotlinx.android.synthetic.main.fragment_player_list.*
import org.jetbrains.anko.support.v4.startActivity

class PlayerListFragment : MvpFragment<PlayerListPresenter>(), PlayerListView {


    override fun onSuccess(it: ListPlayer?) {
        val models = it?.player
        val adapter = PlayerListRecyclerAdapter(models){
            startActivity<DetailPlayerActivity>(IntentParams.ID_PLAYER to it?.idPlayer.toString())
        }
        recyclerPlayerList.apply {
            this.adapter = adapter
            layoutManager = LinearLayoutManager(context)
        }
        adapter.notifyDataSetChanged()
    }

    override fun doCreateFragment() {
        mPresenter.getListPlayer(activity?.intent?.getStringExtra(IntentParams.ID_LEAGUE))
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_player_list
    }

    override fun createPresenter(): PlayerListPresenter {
        val service = RetrofitClient.request().create(RetrofitService::class.java)
        return PlayerListPresenter(this, service)
    }
}