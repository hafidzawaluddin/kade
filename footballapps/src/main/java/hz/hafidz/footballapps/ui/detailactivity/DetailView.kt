package hz.hafidz.footballapps.ui.detailactivity

import hz.hafidz.footballapps.network.dao.DetailEvent
import hz.hafidz.footballapps.network.dao.EventsItem

interface DetailView {
    fun onDetailMatchSuccess(detailEvent: DetailEvent)
    fun onGetTeamImageAway(badge: String?)
    fun onGetTeamImageHome(strTeamBadge: String?)
}