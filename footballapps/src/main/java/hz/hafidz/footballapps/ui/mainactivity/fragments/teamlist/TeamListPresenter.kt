package hz.hafidz.footballapps.ui.mainactivity.fragments.teamlist

import android.annotation.SuppressLint
import hz.hafidz.footballapps.base.BasePresenter
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TeamListPresenter(teamListView: TeamListView, retrofitService: RetrofitService): BasePresenter<TeamListView>(){
    init {
        attachView(teamListView, retrofitService)
    }

    @SuppressLint("CheckResult")
    fun getTeam(id: String?){
        mService.getListTeam(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    mView?.onSuccess(it.teams)
                }

    }

    @SuppressLint("CheckResult")
    fun getLeague(){
        mService.getListLeague()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    mView?.onSuccessTeam(it)
                }
    }

    @SuppressLint("CheckResult")
    fun getSearch(team:String?){
        mService.getSearchTeam(team)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe {
                    mView?.onSearchSuccess(it)
                }
    }
}