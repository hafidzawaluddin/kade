package hz.hafidz.footballapps.network.dao

import com.google.gson.annotations.SerializedName

data class ListTeam(

	@field:SerializedName("teams")
	val teams: List<TeamsItem?>? = null
)