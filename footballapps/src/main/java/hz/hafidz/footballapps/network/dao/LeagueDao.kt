package hz.hafidz.footballapps.network.dao

import com.google.gson.annotations.SerializedName

data class LeagueDao(

	@field:SerializedName("leagues")
	val leagues: List<LeaguesItem?>? = null
)