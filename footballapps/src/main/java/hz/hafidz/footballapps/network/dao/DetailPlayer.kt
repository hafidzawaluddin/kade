package hz.hafidz.footballapps.network.dao

import com.google.gson.annotations.SerializedName

data class DetailPlayer(

	@field:SerializedName("players")
	val players: List<PlayersItem?>? = null
)