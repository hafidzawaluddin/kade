package hz.hafidz.footballapps.network.dao

import com.google.gson.annotations.SerializedName

data class MatchSearch(

	@field:SerializedName("event")
	val event: MutableList<MatchDaoList?>? = null
)