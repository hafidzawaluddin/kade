package hz.hafidz.footballapps.network.dao

import com.google.gson.annotations.SerializedName

data class DetailTeam(

        @field:SerializedName("teams")
        val teams: List<TeamsItem?>? = null
)