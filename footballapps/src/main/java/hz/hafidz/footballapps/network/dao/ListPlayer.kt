package hz.hafidz.footballapps.network.dao

import com.google.gson.annotations.SerializedName

data class ListPlayer(

	@field:SerializedName("player")
	val player: List<PlayerItem?>? = null
)