package hz.hafidz.footballapps.network.dao

import com.google.gson.annotations.SerializedName

data class MatchDao(
@SerializedName("events") val events: List<MatchDaoList>?
)