package hz.hafidz.footballapps.network.dao

import com.google.gson.annotations.SerializedName

data class DetailEvent(

	@field:SerializedName("events")
	val events: List<EventsItem?>? = null
)