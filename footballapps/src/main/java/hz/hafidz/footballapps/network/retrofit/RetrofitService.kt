package hz.hafidz.footballapps.network.retrofit

import hz.hafidz.footballapps.network.dao.*
import hz.hafidz.footballapps.util.Network
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {
    @GET(Network.NEXT_EVENT)
    fun getNextMatch(@Query(Network.ID) id: String): Observable<MatchDao>

    @GET(Network.LAST_EVENT)
    fun getLastMatch(@Query(Network.ID) id: String?): Observable<MatchDao>

    @GET(Network.TEAM_DETAIL)
    fun getDetailTeam(@Query(Network.ID) id: String?): Observable<DetailTeam>

    @GET(Network.DETAIL_EVENT)
    fun getDetailEvent(@Query(Network.ID) id: String?): Observable<DetailEvent>

    @GET(Network.LIST_TEAM)
    fun getListTeam(@Query(Network.ID) id: String?): Observable<ListTeam>

    @GET(Network.LIST_LEAGUE)
    fun getListLeague(): Observable<LeagueDao>

    @GET(Network.LIST_PLAYER)
    fun getListPlayer(@Query(Network.ID) id: String?): Observable<ListPlayer>

    @GET(Network.PLAYERS_DETAIL)
    fun getPlayerDetail(@Query(Network.ID) id: String?): Observable<DetailPlayer>

    @GET(Network.SEARCH_TEAM)
    fun getSearchTeam(@Query(Network.T) team: String?): Observable<ListTeam>

    @GET(Network.SEARCH_EVENT)
    fun getSearchEvent(@Query(Network.E) event:String?): Observable<MatchSearch>
}