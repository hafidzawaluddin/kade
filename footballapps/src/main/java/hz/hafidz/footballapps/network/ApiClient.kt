package hz.hafidz.footballapps.network

import java.net.URL

class ApiClient {
    fun doRequest(url: String): String {
        return URL(url).readText()
    }
}