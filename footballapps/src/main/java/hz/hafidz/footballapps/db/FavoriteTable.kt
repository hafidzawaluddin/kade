package hz.hafidz.footballapps.db

data class FavoriteTable(
        val id: Int?,
        val idEvent: Int?,
        val teamHome: String?,
        val teamAway: String?,
        val scoreHome: String?,
        val scoreAway: String?,
        val eventDate: String?) {

    companion object {
        const val TABLE_NAME = "favorite"
        const val ID = "id"
        const val ID_EVENT = "idEvent"
        const val TEAM_HOME = "teamHome"
        const val TEAM_AWAY = "teamAway"
        const val SCORE_HOME = "scoreHome"
        const val SCORE_AWAY = "scoreAway"
        const val EVENT_DATE = "eventDate"
    }
}