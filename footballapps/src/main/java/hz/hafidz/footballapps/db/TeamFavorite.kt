package hz.hafidz.footballapps.db

data class TeamFavorite(
        val id: Int?,
        val idTeam: Int?,
        val img: String?,
        val teamName: String?
) {
    companion object {
        const val TABLE_NAME = "favoriteteam"
        const val ID = "id"
        const val ID_TEAM = "idTeam"
        const val IMAGE = "image"
        const val TEAM_NAME = "teamName"
    }
}