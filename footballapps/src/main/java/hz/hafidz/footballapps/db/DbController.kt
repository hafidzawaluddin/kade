package hz.hafidz.footballapps.db

import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class DbController(private val database: SQLiteDatabase) {

    fun getFavorite(): List<FavoriteTable> {
        return database.select(FavoriteTable.TABLE_NAME)
                .parseList(classParser())
    }

    fun insertFavorite(eventId: String?, homeTeam: String?, awayTeam: String?,
                       homeScore: String?, awayScore: String?, date: String?): Boolean {
        return try {
            database.insert(FavoriteTable.TABLE_NAME,
                    FavoriteTable.ID to null,
                    FavoriteTable.ID_EVENT to eventId,
                    FavoriteTable.TEAM_HOME to homeTeam,
                    FavoriteTable.TEAM_AWAY to awayTeam,
                    FavoriteTable.SCORE_HOME to homeScore,
                    FavoriteTable.SCORE_AWAY to awayScore,
                    FavoriteTable.EVENT_DATE to date)
            true
        } catch (e: Exception) {
            false
        }

    }

    fun deleteFavorite(id: String?): Boolean {
        return try {
            database.delete(FavoriteTable.TABLE_NAME,
                    "${FavoriteTable.ID_EVENT} = $id")
            true
        } catch (e: Exception) {
            false
        }
    }

    fun isFavorite(id: String?): Boolean {
        return try {
            val result = database.select(FavoriteTable.TABLE_NAME)
                    .whereArgs("${FavoriteTable.ID_EVENT} = $id")
                    .parseList(classParser<FavoriteTable>())
            println(result[0].idEvent)
            result.isNotEmpty()
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    fun getFavoriteTeam(): List<TeamFavorite> {
        return database.select(TeamFavorite.TABLE_NAME)
                .parseList(classParser())
    }

    fun insertTeamFavorite(teamId: String?, img: String?, teamName: String?): Boolean {
        return try {
            database.insert(TeamFavorite.TABLE_NAME,
                    TeamFavorite.ID to null,
                    TeamFavorite.ID_TEAM to teamId,
                    TeamFavorite.IMAGE to img,
                    TeamFavorite.TEAM_NAME to teamName)
            true
        } catch (e: Exception){
            false
        }
    }

    fun deleteFavoriteTeam(idEvent:String?): Boolean{
        return try {
            database.delete(TeamFavorite.TABLE_NAME,
                    "${TeamFavorite.ID_TEAM} = $idEvent")
            true
        } catch (e:Exception){
            false
        }
    }

    fun isTeamFavorite(idTeam: String?): Boolean{
        return try {
            val result = database.select(TeamFavorite.TABLE_NAME)
                    .whereArgs("${TeamFavorite.ID_TEAM} = $idTeam")
                    .parseList(classParser<FavoriteTable>())
            println(result[0].idEvent)
            result.isNotEmpty()
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }
}