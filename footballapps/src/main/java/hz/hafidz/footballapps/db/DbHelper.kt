package hz.hafidz.footballapps.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import org.jetbrains.anko.db.*

class DbHelper(context: Context) : SQLiteOpenHelper(context, "fav", null, 7) {
    companion object {
        private var instance: DbHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DbHelper {
            if (instance == null) {
                instance = DbHelper(ctx.applicationContext)
            }
            return instance as DbHelper
        }
    }


    override fun onCreate(p0: SQLiteDatabase?) {
        p0?.createTable(FavoriteTable.TABLE_NAME, true,
                FavoriteTable.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                FavoriteTable.ID_EVENT to INTEGER + UNIQUE,
                FavoriteTable.TEAM_HOME to TEXT,
                FavoriteTable.TEAM_AWAY to TEXT,
                FavoriteTable.SCORE_HOME to TEXT,
                FavoriteTable.SCORE_AWAY to TEXT,
                FavoriteTable.EVENT_DATE to TEXT)

        p0?.createTable(TeamFavorite.TABLE_NAME, true,
                TeamFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                TeamFavorite.ID_TEAM to INTEGER + UNIQUE,
                TeamFavorite.IMAGE to TEXT,
                TeamFavorite.TEAM_NAME to TEXT)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0?.dropTable(FavoriteTable.TABLE_NAME, true)
        p0?.dropIndex(TeamFavorite.TABLE_NAME, true)
        onCreate(p0)
    }

    fun getController(): DbController = DbController(readableDatabase)

}

val Context.database: DbController
    get() = DbHelper.getInstance(applicationContext).getController()