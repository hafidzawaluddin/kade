package hz.hafidz.footballapps.upcoming

import hz.hafidz.footballapps.BuildConfig
import hz.hafidz.footballapps.network.dao.MatchDao
import hz.hafidz.footballapps.network.dao.MatchDaoList
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.mainactivity.fragments.comingfragment.UpComingPresenter
import hz.hafidz.footballapps.ui.mainactivity.fragments.comingfragment.UpComingView
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class UpComingPresenterTest {

    @Mock
    private lateinit var retrofitService:RetrofitService

    @Mock
    private lateinit var view:UpComingView

    lateinit var presenter:UpComingPresenter

    lateinit var match : MatchDao

    lateinit var matchDao : Observable<MatchDao>

    private var result:List<MatchDaoList>? = null

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)

        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

        presenter = UpComingPresenter(view, retrofitService)

        match = MatchDao(result)
        matchDao = Observable.just(match)

        Mockito.`when`(retrofitService.getNextMatch(BuildConfig.LEAGUE)).thenReturn(matchDao)
    }

    @Test
    fun testGetMatchList() {
        presenter.getNextMatch(BuildConfig.LEAGUE)
        Mockito.verify(view).showLoading()
        Mockito.verify(view).onSuccess(result)
        Mockito.verify(view).hideLoading()
    }
}