package hz.hafidz.footballapps.upcoming

import hz.hafidz.footballapps.network.dao.DetailEvent
import hz.hafidz.footballapps.network.dao.EventsItem
import hz.hafidz.footballapps.network.retrofit.RetrofitService
import hz.hafidz.footballapps.ui.detailactivity.DetailPresenter
import hz.hafidz.footballapps.ui.detailactivity.DetailView
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DetailPresenterTest {
    @Mock
    private lateinit var retrofitService: RetrofitService

    @Mock
    private lateinit var view: DetailView

    lateinit var presenter: DetailPresenter

    lateinit var event : DetailEvent

    lateinit var detailEventDao : Observable<DetailEvent>

    private var result:List<EventsItem>? = null

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)

        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

        presenter = DetailPresenter(view, retrofitService)

        event = DetailEvent(result)
        detailEventDao = Observable.just(event)

        Mockito.`when`(retrofitService.getDetailEvent("4328")).thenReturn(detailEventDao)
    }

    @Test
    fun testGetMatchList() {
        presenter.getDetailMatch("4328")
        Mockito.verify(view).onDetailMatchSuccess(event)
    }
}